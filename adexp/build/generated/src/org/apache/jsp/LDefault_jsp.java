package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class LDefault_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {


          
//   Filename: TemplateObj.jsp
//   Generated with CodeCharge <xsl:value-of select="/Site/@CCVersion"/>
//   JSPTemplates build 05/21/2001
//         Usage:
//             TextTemplate t = new TextTemplate();
//             t.LoadTemplate ("usr/loca/apache/htdocs/example/templates/new.html"), "main");
//             t.setVar ("ID", 2);
//             t.setVar ("Value", "Name");
//             t.parse ("DynBlock", false ); //or True if you want to create a list
//             t.parse ("main", false);
//             t.printVar ("main");

public class TextTemplate {

  private java.util.Properties dBlocks = new java.util.Properties();
  private java.util.Properties parsedBlocks = new java.util.Properties();

  public void setBlock(String tplName, String blockName) {
    dBlocks.put(blockName, getBlock(dBlocks.getProperty(tplName), blockName));
    dBlocks.put(tplName, replaceBlock(dBlocks.getProperty(tplName), blockName));
    String nName = nextDBlockName(blockName);
    while (! "".equals(nName)) {
      setBlock(blockName, nName);
      nName = nextDBlockName(blockName);
    }
  }

  public void loadTemplate(String path, String name) {
    try {
      String fileContent = null;
      try {
        fileContent = getFileContent(path);
      }
      catch (java.io.FileNotFoundException e) {
        System.err.println("\nCodecharge "+new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date())+" ==> \""+path+"\" not found.\n\n");
      }
      catch (java.io.IOException e) {}
      if (fileContent != null) {
        dBlocks.put(name, fileContent);
        String nName = nextDBlockName(name);
        while (! (nName == null || "".equals(nName))) {
          setBlock(name, nName);
          nName = nextDBlockName(name);
        }
      }
    }
    catch ( Exception e ) {
      System.err.println("\nCodecharge "+new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date())+" ==> TextTemplate.loadTemplate(\""+path+"\",\""+name+"\")\n\n");
    }
  }

  public String getVar(String name) {
    String result = dBlocks.getProperty(name);
    if (result==null) {
      return "";
    }
    else {
      return result;
    }
  }

  public void setVar(String name, String value) {
    if ( value == null ) value =""; 
    parsedBlocks.put(name, value);
  }

  public void parse(String tplName, boolean repeat) {
    try {
      if (parsedBlocks.getProperty(tplName) != null) {
        if (repeat) {
          parsedBlocks.put(tplName, parsedBlocks.getProperty(tplName) + proceedTpl(dBlocks.getProperty(tplName)));
          return;
        } 
        else {
          parsedBlocks.put(tplName, proceedTpl(dBlocks.getProperty(tplName)));
        }
      }
      else {
        parsedBlocks.put(tplName, proceedTpl(dBlocks.getProperty(tplName)));
      }
    }
    catch ( Exception e ) {
      System.err.println("\nCodecharge "+new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date())+" ==> TextTemplate.parse("+tplName+","+repeat+")\n\n");
      e.printStackTrace(System.err);
      System.err.println("\nparsedBlocks.getProperty("+tplName+") => "+parsedBlocks.getProperty(tplName));
      System.err.println("dBlocks.getProperty("+tplName+") => "+dBlocks.getProperty(tplName));
    }
  }

  public String printVar(String name) {
    return parsedBlocks.getProperty(name);
  }

  private String proceedTpl(String tpl) {
    String tTpl = tpl;
    try {
      String regEx = null, match = null, name = null;
      java.util.Enumeration matches = null;
      matches = extractMatches(tpl, '{', '}');
      while (matches.hasMoreElements()) {
        match = (String)matches.nextElement();
        name = match.substring(1, match.length() - 1);
        if (parsedBlocks.containsKey(name)) {
          tTpl = replace(tTpl, match, parsedBlocks.getProperty(name));
        }
        else {
          tTpl = replace(tTpl, match, dBlocks.getProperty(name));
        }
      }
    }
    catch ( Exception e ) {
      System.err.println("\nCodecharge "+new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date())+" ==> TextTemplate.proceedTpl("+tpl+")\n\n");
      e.printStackTrace(System.err);
    }
    return tTpl;
  }

  private String getBlock(String template, String name) {
    int bBlock, eBlock, alpha;
    alpha = name.length() + 12;
    bBlock = template.indexOf("<!--Begin" + name + "-->");
    eBlock = template.indexOf("<!--End" + name + "-->");
    if (! (bBlock == 0 || eBlock == 0)) {
        return template.substring(bBlock + alpha, eBlock); 
    }
    else {
        return "";
    }
  }

  private String replaceBlock(String template, String name) {
    int bBlock, eBlock;
    bBlock = template.indexOf("<!--Begin" + name + "-->");
    eBlock = template.indexOf("<!--End" + name + "-->");
    if (! (bBlock == 0 || eBlock == 0)) {
      return template.substring(0, bBlock) + "{" + name + "}" +
             template.substring(eBlock + ("<!--End" + name + "-->").length(), template.length());
    }
    else {
      return template;
    }
  }

  private String nextDBlockName(String templateName) {
    int bTag = 0, eTag = 0;
    String name = "", template = "";
    template = dBlocks.getProperty(templateName);
  
    bTag = template.indexOf("<!--Begin");
    if (bTag != -1) {
      eTag = template.indexOf("-->", bTag);
      name = template.substring(bTag + 9, eTag);
      if (template.indexOf("<!--End" + name + "-->") != -1) {
        return name;
      } 
    }
    return "";
  }

  public String printAll() {
    return "<h1><font color=\"red\">printAll() is not implemented yet</font></h1>";
  }

  private java.util.Enumeration extractMatches(String str, char start, char end) {
    java.util.Vector vec = new java.util.Vector(10, 10);
    if (str != null) {
      int s = 0, e = 0, sc = 0, se = 0;
      while ((s = str.indexOf(start, e)) >= 0) {
        if ((e = str.indexOf(end, s)) >= 0) {
          sc = str.indexOf(" ", s);
          se = str.indexOf("\n", s);
          if ( sc == -1 ) { sc = se; }
          if ( se < sc && se != -1 ) { sc = se; }
          se = str.indexOf("=", s);
          if ( sc == -1 ) { sc = se; }
          if ( se < sc && se != -1 ) { sc = se; }
          if ( sc == -1 || sc > e ) {
            vec.addElement(str.substring(s, e) + end);
          }
        }
      }
    }
    return vec.elements();
  }
     
  private String replace(String str, String pattern, String replace) {
    if (replace == null) {
      replace = "";
    }
    int s = 0, e = 0;
    StringBuffer result = new StringBuffer();
    while ((e = str.indexOf(pattern, s)) >= 0) {
      result.append(str.substring(s, e));
      result.append(replace);
      s = e + pattern.length();
    }
    result.append(str.substring(s));
    return result.toString();
  }

  private String getFileContent(String fName) throws java.io.FileNotFoundException, java.io.IOException {
    java.io.File f = new java.io.File (fName);
    int lenFile = (int) f.length(); //No checking so template-file is not big
    java.io.BufferedReader bf = new java.io.BufferedReader(new java.io.FileReader(fName));
    StringBuffer line = new StringBuffer(lenFile+1024);
    while (bf.ready()) {    
      line.append(bf.readLine() + "\n");
    }
    bf.close();
    return line.toString();
  }
}



//   Filename: Styles.inc
//   Generated with CodeCharge 1.2.0
//   JSPTemplates build 05/21/2001



String stylePageBODY = "bgcolor=\"#FFFFFF\" text=\"#000000\" link=\"#0000FF\" vlink=\"#800080\" alink=\"#FF0000\"";
String styleFormTABLE = "cellpadding=\"0\" border=\"0\" cellspacing=\"0\"";
String styleFormHeaderTD = "align=\"center\" bgcolor=\"#0033cc\"";
String styleFormHeaderFONT = "style=\"font-size: 12pt; color: #FFFFFF; font-family: Arial, Tahoma, Verdana, Helvetica; font-weight: bold\"";
String styleFieldCaptionTD = "bgcolor=\"#DEEFFF\"";
String styleFieldCaptionFONT = "style=\"font-size: 10pt; color: #000000; font-family: Arial, Tahoma, Verdana, Helvetica; font-weight: bold\"";
String styleDataTD = "bgcolor=\"#99CCFF\"";
String styleDataFONT = "style=\"font-size: 10pt; color: #000000; font-family: Arial, Tahoma, Verdana, Helvetica\"";
String styleColumnFONT = "style=\"font-size: 10pt; color: #000000; font-family: Arial, Tahoma, Verdana, Helvetica; font-weight: bold\"";
String styleColumnTD = "bgcolor=\"#DEEFFF\"";




//
//   Filename: Common.jsp
//   Generated with CodeCharge  v.1.2.0
//   JSPTemplates build 05/21/2001
//

  static final String CRLF = "\r\n";

  static final int RECORDS_PER_PAGE = 20;

  static final int UNDEFINT=Integer.MIN_VALUE;
  static final int Text_TYPE = 1;
  static final int Date_TYPE = 2;
  static final int Number_TYPE = 3; 
  static final int _Search__TYPE = 4;
  static final int Search_TYPE = 5;
  static final int adText = Text_TYPE ;
  static final int adNumber = Number_TYPE ;


//Database connection string

  static final String DBDriver  ="com.mysql.jdbc.Driver";
  static final String strConn   ="jdbc:mysql://localhost:3306/adxpress";
  static final String DBmembername="root";
  static final String DBpassword="root";

  public void addSessionError(String errName, String errText, javax.servlet.http.HttpSession session){
    String oldErr = (String) session.getAttribute(errName);
    oldErr += errText;
    session.putValue(errName, oldErr);
  }

  public static String loadDriver () {
    String sErr = "";
    try {
      java.sql.DriverManager.registerDriver((java.sql.Driver)(Class.forName(DBDriver).newInstance()));
    }
    catch (Exception e) {
      sErr = e.toString();
    }
    return (sErr);
  }

  public static void absolute(java.sql.ResultSet rs, int row) throws java.sql.SQLException{
    for(int x=1;x<row;x++) rs.next();
  }

  java.sql.ResultSet openrs(java.sql.Statement stat, String sql) throws java.sql.SQLException {
    java.sql.ResultSet rs = stat.executeQuery(sql);
    return (rs);
  }

  String toURL(String strValue){
    if ( strValue == null ) return "";
    if ( strValue.compareTo("") == 0 ) return "";
    return java.net.URLEncoder.encode(strValue);
  }

  String[] getFieldsName ( java.sql.ResultSet rs ) throws java.sql.SQLException {
    java.sql.ResultSetMetaData metaData = rs.getMetaData();
    int count = metaData.getColumnCount();
    String[] aFields = new String[count];
    for(int j = 0; j < count; j++) {
      aFields[j] = metaData.getColumnLabel(j+1);
    }
    return aFields;
  }

  java.util.Hashtable getRecordToHash ( java.sql.ResultSet rs, java.util.Hashtable rsHash, String[] aFields ) throws java.sql.SQLException {
    for ( int iF = 0; iF < aFields.length; iF++ ) {
      rsHash.put( aFields[iF], getValue(rs, aFields[iF]));
    }
    return rsHash;
  }

  long dCountRec(java.sql.Statement stat, String table, String sWhere) {
    long lNumRecs = 0;
    try {
      java.sql.ResultSet rs = stat.executeQuery("select count(*) from " + table + " where " + sWhere);
      if ( rs != null && rs.next() ) {
        lNumRecs = rs.getLong(1);
      }
      rs.close();
    }
    catch (Exception e ) {};
    return lNumRecs;
  }

  String getCheckBoxValue(String sVal, String CheckedValue, String UnCheckedValue, int sType) {
    if (isEmpty(sVal)) {
      return toSQL(UnCheckedValue, sType);
    }
    else {
      return toSQL(CheckedValue, sType);
    }
  }

  String dLookUp(java.sql.Statement stat, String table, String fName, String where) {
    try{
System.out.println("dLookup="+stat+ "SELECT " + fName + " FROM " + table + " WHERE " + where);
      java.sql.ResultSet rs = openrs(stat, "SELECT " + fName + " FROM " + table + " WHERE " + where);
      if (! rs.next()) {
        rs.close();
        return "";
      }
      String res = rs.getString(1);
      rs.close();
      return (res == null ? "" : res);
    }
    catch (Exception e) {
      return "";
    }
  }

  String proceedError(javax.servlet.http.HttpServletResponse response, Exception e) {
    return e.toString();
  }


  String CheckSecurity(javax.servlet.http.HttpSession session, javax.servlet.http.HttpServletResponse response, javax.servlet.http.HttpServletRequest request, String scriptName) {
    try{
      if (session.getAttribute("UserID")==null) {
        response.sendRedirect("Login.jsp?querystring=" + toURL(request.getQueryString()) + "&ret_page=" + scriptName);
        return "sendRedirect";
      }
    }
    catch(Exception e){};
    return "";
  }

  String CheckSecurity(int iLevel, javax.servlet.http.HttpSession session, javax.servlet.http.HttpServletResponse response, javax.servlet.http.HttpServletRequest request, String scriptName){
    try {
      Object o1 = session.getAttribute("UserID");
      Object o2 = session.getAttribute("UserRights");
      boolean bRedirect = false;
      if ( o1 == null || o2 == null ) { bRedirect = true; }
      if ( ! bRedirect ) {
        if ( (o1.toString()).equals("")) { bRedirect = true; }
        else if ( (new Integer(o2.toString())).intValue() < iLevel) { bRedirect = true; }
      }

      if ( bRedirect ) {
        response.sendRedirect("Login.jsp?querystring=" + toURL(request.getQueryString()) + "&ret_page=" + toURL(request.getRequestURI()));
        return "sendRedirect";
      }
    }
    catch(Exception e){};
    return "";
  }



  java.sql.Connection cn() throws java.sql.SQLException {
    return java.sql.DriverManager.getConnection(strConn , DBmembername, DBpassword);
  }

  String toHTML(String value) {
    if ( value == null ) return "";
    value = replace(value, "&", "&amp;");
    value = replace(value, "<", "&lt;");
    value = replace(value, ">", "&gt;");
    value = replace(value, "\"", "&" + "quot;");
    return value;
  }

  String getValueHTML(java.sql.ResultSet rs, String fieldName){
    return  toHTML(getValue(rs, fieldName));
  }

  String getValue(java.sql.ResultSet rs, String fieldName) {
   if ((rs==null) ||(isEmpty(fieldName)) || ("".equals(fieldName))) return "";
     try {
       String value = rs.getString(fieldName);
       if (value != null) {
         return (value);
       }
     }
     catch (java.sql.SQLException sqle) {}
     return "";
  }

  String getParam(javax.servlet.http.HttpServletRequest req, String paramName, int paramType) {
    String param = req.getParameter(paramName);
    switch (paramType){
      case Number_TYPE: return (isNumber(param)?param:String.valueOf(UNDEFINT)); 
      case Text_TYPE:
      case _Search__TYPE:
      case Search_TYPE:  return (param == null ? "" : param);
    }
    return "";
  }

  String getParam(javax.servlet.http.HttpServletRequest req, String paramName) {
    String param = req.getParameter(paramName);
    if (param==null) return "";
    else return param;
  }

  boolean isNumber (String param) {
    boolean result;
    if ( param == null || param.equals("")) return true;
    param=param.replace('d','_').replace('f','_');
    try {
      Double dbl = new Double(param);
      result = true;
    }
    catch (NumberFormatException nfe) {
      result = false;
    }
    return result;
  }

  boolean isEmpty (int val){
    return val==UNDEFINT;
  }

  boolean isEmpty (String val){
    return (val==null || val.equals("")||val.equals(Integer.toString(UNDEFINT))); 
  }

  String toWhereSQL(String fieldName, String fieldVal, int type) {
    String res = "";
    switch(type) {
      case Text_TYPE: 
        if (! "".equals(fieldVal)) {
              res = " " + fieldName + " like '%" + fieldVal + "%'";
            }
      case Number_TYPE:
        res = " " + fieldName + " = " + fieldVal + " ";
      case Date_TYPE:
        res = " " + fieldName + " = '" + fieldVal + "' ";
      default:
        res = " " + fieldName + " = '" + fieldVal + "' ";
    }
    return res;
  }

  String toSQL(String value, int type) {
    if ( value == null ) return "Null";
    String param = value;
    if ("".equals(param) && (type == Text_TYPE || type == Date_TYPE) ) {
      return "Null";
    } 
    switch (type) {
      case Text_TYPE: {
        param = replace(param, "'", "''");
        param = replace(param, "&amp;", "&");
        param = "'" + param + "'";
        break;
      }
      case _Search__TYPE:
      case Search_TYPE: {
        param = replace(param, "'", "''");
        break;
      }
      case Number_TYPE: {
        try {
          if (! isNumber(value) || "".equals(param)) param="null";
          else param = value;
        }
        catch (NumberFormatException nfe) {
          param = "null";
        }
        break;
      }
      case Date_TYPE: {
        param = "'" + param + "'";
        break;      
      }
    }
    return param;
  }

  private String replace(String str, String pattern, String replace) {
    if (replace == null) {
      replace = "";
    }
    int s = 0, e = 0;
    StringBuffer result = new StringBuffer();
    while ((e = str.indexOf(pattern, s)) >= 0) {
      result.append(str.substring(s, e));
      result.append(replace);
      s = e + pattern.length();
    }
    result.append(str.substring(s));
    return result.toString();
  }

  private String getFileContent(String fName) throws java.io.FileNotFoundException, java.io.IOException {
    java.io.BufferedReader bf = new java.io.BufferedReader(new java.io.FileReader(fName));
    String line="";
    while (bf.ready()) {    
        line += bf.readLine() +"\n";
    }
    bf.close();
    return line;
  }

  void getOptions( java.sql.Connection conn, String sql, boolean isSearch, boolean isRequired, String selectedValue, String nameBlock, TextTemplate oTpl ) {
    oTpl.setVar ( nameBlock, "");
    if ( isSearch ) {
      oTpl.setVar ("ID", "");
      oTpl.setVar ("Value", "All");
      oTpl.parse ( nameBlock, true);
    }
    else {
      if ( ! isRequired ) {
        oTpl.setVar ("ID", "");
        oTpl.setVar ("Value", "");
        oTpl.parse ( nameBlock, true);
      }
    }
    java.sql.ResultSet rs = null;
    java.sql.Statement stat = null;
    try{
      stat = conn.createStatement();
      rs = openrs (stat, sql);
      while (rs.next()){
        String lbId  = rs.getString(1);
        String lbVal = rs.getString(2);
        if (lbId!=null){
          oTpl.setVar ("ID", lbId);
          if (lbVal!=null){
            oTpl.setVar ("Value", lbVal);
          }
          else oTpl.setVar ("Value", "");
          if (lbId.equals(""+selectedValue)) {
            oTpl.setVar ("Selected", "SELECTED");
          }
          else oTpl.setVar ("Selected", "");
          oTpl.parse ( nameBlock, true);
        }
      }
      rs.close();
      stat.close();
    }
    catch(Exception ignore)  {}
  }

  void getOptionsLOV( String sLOV, boolean isSearch, boolean isRequired, String selectedValue, String nameBlock, TextTemplate oTpl ) {
    String id = "";
    String val = "";
    java.util.StringTokenizer LOV = new java.util.StringTokenizer(sLOV, ";", true);
    int i = 0;
    String old = ";";
    while ( LOV.hasMoreTokens() ) {
      id = LOV.nextToken();
      if (!old.equals(";") && (id.equals(";"))) id = LOV.nextToken();
      else if (old.equals(";") && (id.equals(";"))) id = "";
      if (!id.equals("")) old = id;
      i++;
      if (LOV.hasMoreTokens()) {
        val = LOV.nextToken();
        if (!old.equals(";") && (val.equals(";"))) val = LOV.nextToken();
        else if (old.equals(";")&& (val.equals(";"))) val = "";
        if ( val.equals(";") ) { val = ""; }
        if (!val.equals("")) old = val;
        i++;
      }
      if ( id.compareTo(selectedValue) == 0 ) {
        oTpl.setVar ("Selected", "SELECTED");
      } else {
        oTpl.setVar ("Selected", "");
      }
      oTpl.setVar ("ID", id);
      oTpl.setVar ("Value", val);
      oTpl.parse ( nameBlock, true);
    }
  }

  String getValFromLOV( String selectedValue , String sLOV) {
    String sRes = "";
    String id = "";
    String val = "";
    java.util.StringTokenizer LOV = new java.util.StringTokenizer( sLOV, ";", true);
    int i = 0;
    String old = ";";
    while ( LOV.hasMoreTokens() ) {
      id = LOV.nextToken();
      if ( ! old.equals(";") && ( id.equals(";") ) ) {
        id = LOV.nextToken();
      }
      else {
        if ( old.equals(";") && ( id.equals(";") ) ) {
          id = "";
        }
      }
      if ( ! id.equals("") )  { old = id; }

      i++;

      if (LOV.hasMoreTokens()) {
        val = LOV.nextToken();
        if ( ! old.equals(";") && (val.equals(";") ) ) {
          val = LOV.nextToken();
        }
        else {
          if (old.equals(";") && (val.equals(";"))) {
            val = "";
          }
        }
        if ( val.equals(";") ) { val = ""; }
        if ( ! val.equals("")) { old = val; }
        i++;
      }

      if ( id.compareTo( selectedValue ) == 0 ) {
        sRes = val;
      }
    } // End while
    return sRes;
  }




//   Filename: Default.jsp
//   Generated with CodeCharge 1.2.0
//   JSPTemplates build 05/21/2001

String sFileName = "LDefault.jsp";



void Search_Show( javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, javax.servlet.http.HttpSession session, javax.servlet.jsp.JspWriter out, String sForm, String sAction, java.sql.Connection conn, java.sql.Statement stat, TextTemplate oTpl) throws java.io.IOException {



      String flddescription="";

  String sSQL = "";
      
  oTpl.setVar ("ActionPage", "LItemsList.jsp");

  // Set variables with search parameters
  
  flddescription = getParam(request, "description");
  // Show fields
      
oTpl.setVar ("description", toHTML(flddescription));
  oTpl.parse ("FormSearch", false);
}





void Lates_Show( javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, javax.servlet.http.HttpSession session, javax.servlet.jsp.JspWriter out, String sForm, String sAction, java.sql.Connection conn, java.sql.Statement stat, TextTemplate oTpl) throws java.io.IOException {
  

boolean bReq =false;

String sWhere = "";

boolean HasParam = false;





  String sOrder="";
  String sDirection = "";
  String sSortParams = "";
  String sSQL="";


  oTpl.setVar ("TransitParams", "");
  oTpl.setVar ("FormParams", "");
  // Build WHERE statement

  sWhere = " WHERE approved=1";
  
  // Build ORDER statement
  sOrder = " order by l.add_date Desc";

  // Build full SQL statement

  sSQL = "select l.add_date as l_add_date, " +
    "l.link_id as l_link_id, " +
    "l.name as l_name, " +
    "l.url as l_url " +
    " from links l ";
  
  sSQL = sSQL + sWhere + sOrder;
    // Select current page
    int iPage;
    try{
      iPage = Integer.parseInt(getParam(request, "FormLates_Page", Number_TYPE));
    }
    catch(Exception e){iPage =1;}
    if (iPage < 1) iPage = 1;

    int RecordsPerPage = 5;
    int startRec = (iPage-1)*RecordsPerPage+1;
  
  java.sql.ResultSet rs = null;
  try{
    // Open recordset
    rs = openrs (stat, sSQL);
    if ((rs==null) || (!rs.next())) {
      // Recordset is empty
      if ( rs != null ) rs.close();
      oTpl.setVar ("DListLates", "");
      oTpl.parse ("LatesNoRecords", false);
      oTpl.setVar ("LatesScroller", "");
      oTpl.parse ("FormLates", false);
      return;
    }

    absolute(rs,startRec);
    int iCounter = 0;
    boolean EOF=false;

    String[] aFields = getFieldsName( rs );
    java.util.Hashtable rsHash = new java.util.Hashtable();

    // Show main table based on recordset
    while ((!EOF)  && (iCounter < RecordsPerPage) ){

      getRecordToHash( rs, rsHash, aFields );
      String fldadd_date = (String) rsHash.get("l_add_date");
      String fldlink_id = (String) rsHash.get("l_link_id");
      String fldname = (String) rsHash.get("l_name");
oTpl.setVar ("link_id", toHTML(fldlink_id));
  oTpl.setVar ("name", toHTML(fldname));
  oTpl.setVar ("name_URLLink", (String) rsHash.get("l_url"));
  oTpl.setVar ("add_date", toHTML(fldadd_date));
      oTpl.parse ("DListLates", true);
      
      iCounter++;
      EOF = !rs.next();       
    }
    rs.close();
  
    // Parse scroller
    if ((EOF) && (iPage == 1)) 
      oTpl.setVar ("LatesScroller", "");
    else {
      if (EOF) 
        oTpl.setVar ("LatesScrollerNextSwitch", "_");
      else {
        oTpl.setVar ( "NextPage", Integer.toString((iPage + 1)));
        oTpl.setVar ( "LatesScrollerNextSwitch", "");
      }
      if (iPage == 1) 
        oTpl.setVar ( "LatesScrollerPrevSwitch", "_");
      else {
        oTpl.setVar ( "PrevPage", Integer.toString((iPage - 1)));
        oTpl.setVar ( "LatesScrollerPrevSwitch", "");
      }
      oTpl.setVar ( "LatesCurrentPage", Integer.toString(iPage));
      oTpl.parse ( "LatesScroller", false);
    }
  
  oTpl.setVar ( "LatesNoRecords", "");
  oTpl.parse ( "FormLates", false);
  

  } catch(java.sql.SQLException e){
      out.print("SQL ERROR !"+e);
      oTpl.setVar ("DListLates", "");
      oTpl.parse ("LatesNoRecords", false);
      oTpl.setVar ("LatesScroller", "");
      oTpl.parse ("FormLates", false);
  };
}   


void TreeAction( javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, javax.servlet.http.HttpSession session, javax.servlet.jsp.JspWriter out, String sForm, String sAction, java.sql.Connection conn, java.sql.Statement stat, TextTemplate oTpl) throws java.io.IOException {
  
  String sCatID = getParam(request, "category_id", Number_TYPE);
  if (!isEmpty(sCatID)) {
    if ("0".equals(dLookUp(stat, "links_categories", "count(*)", "parent_category_id=" + sCatID))) {
      try {
        try {
          if ( stat != null )  { stat.close(); stat = null; }
          if ( conn != null ) { conn.close(); conn = null; }
        }
        catch ( java.sql.SQLException ignore ) {}
        response.sendRedirect("LBrowseCategories.jsp?category_id=" +sCatID);
        addSessionError("sDefaultErr", "sendRedirect", session);
        return;
      }
      catch(java.io.IOException ioe) {}
    }   
    
    else {
      try {
       try {
          if ( stat != null )  { stat.close(); stat = null; }
          if ( conn != null ) { conn.close(); conn = null; }
        }
        catch ( java.sql.SQLException ignore ) {}
        response.sendRedirect("LBrowseCategories.jsp?category_id=" + sCatID);
        addSessionError("sDefaultErr", "sendRedirect", session);
        return;
      }
      catch(java.io.IOException ioe) {}
    }
  }
}

void Tree_Show( javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, javax.servlet.http.HttpSession session, javax.servlet.jsp.JspWriter out, String sForm, String sAction, java.sql.Connection conn, java.sql.Statement stat, TextTemplate oTpl) throws java.sql.SQLException
{
  
  String sWhere="";
  java.sql.ResultSet rs=null;
  String sSQL ="";
  String sCatID = "";
  String sParCatID ="";
  oTpl.setVar ("category_id", "");
  oTpl.setVar ("category_desc", "");
  oTpl.setVar ("parent_category_id", "");
  oTpl.setVar ("ActionPage", "LBrowseCategories.jsp");
  sSQL = "select category_id, category_desc, parent_category_id from links_categories";
  sCatID = getParam(request, "category_id", Number_TYPE);
  
  if (isEmpty(sCatID)){
    // Root category
    sWhere = " where parent_category_id is Null";
    oTpl.setVar ("CurrentCategory", "");
    oTpl.setVar ("CatPath", "");
  }
  else {
    // Subcategory
    sWhere = " where category_id=" +sCatID;
    try {
      rs = openrs (stat, sSQL + sWhere);
      if (! rs.next()) {
        rs.close();
        rs = null;
      }
    }
    catch (java.sql.SQLException sqle) {
      rs = null;
    }
    java.util.Hashtable rsHash = new java.util.Hashtable();
    // Build Path
    if ( rs != null ) {
      String[] aFields = getFieldsName( rs );
      getRecordToHash( rs, rsHash, aFields );
      rs.close();
    }
    oTpl.setVar ("CurrentCategory", " > "+ toHTML((String) rsHash.get("category_desc")));
    sParCatID = toHTML((String) rsHash.get("parent_category_id"));
    rs = null;
    if ("".equals(sParCatID)) oTpl.setVar ("CatPath", "");
    String sPath = "";
    while (!"".equals(sParCatID)){
      try {
        rs = openrs (stat, sSQL + " where category_id=" + sParCatID);
        if (! rs.next()) {
          rs.close();
          rs = null;
        }
      }
      catch (java.sql.SQLException sqle) {
        rs = null;
      }
      if ( rs != null ) {
        String[] aFields = getFieldsName( rs );
        getRecordToHash( rs, rsHash, aFields );
        rs.close();
      }
      sParCatID = toHTML((String) rsHash.get("parent_category_id"));
      sPath = replace(replace(oTpl.getVar("CatPath"), "{CategoryID}", toHTML((String) rsHash.get("category_id"))), "{Category}", toHTML((String) rsHash.get("category_desc"))) +sPath;
      rs = null;
    }
    oTpl.dBlocks.put("CatPath", sPath);
    oTpl.parse ("CatPath", false);
    sWhere = " where parent_category_id=" + sCatID;
  }
  try {
    // Categories list
    rs = openrs (stat,  sSQL + sWhere);
    if (! rs.next()) {
      rs.close();
      rs = null;
    }
  } catch (java.sql.SQLException sqle) {
    rs = null;  
  }
  if (rs!=null) {
    boolean EOF=false;
    java.util.Hashtable rsHash = new java.util.Hashtable();
    String[] aFields = getFieldsName( rs );
    // Print subcategories
    while (!EOF){
      getRecordToHash( rs, rsHash, aFields );
      oTpl.setVar ("CategoryID", toHTML((String) rsHash.get("category_id")));
      oTpl.setVar ("Category", toHTML((String) rsHash.get("category_desc")));
    
      oTpl.parse ("CategoryList", true);
      try {
        EOF = !rs.next();
      }
      catch (java.sql.SQLException sqle) {
        EOF = true;
      }
    }
    rs.close();
    oTpl.parse ("FormTree", false);
  
  }
  else{
    // No subcategories
    oTpl.setVar ("CategoryList", "");
    oTpl.parse ("FormTree", false);
  
  }
}


void Menu_Show( javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, javax.servlet.http.HttpSession session, javax.servlet.jsp.JspWriter out, String sForm, String sAction, java.sql.Connection conn, java.sql.Statement stat, TextTemplate oTpl) throws java.io.IOException {
  
  // Set URLs
  
  String fldField1 = "LDefault.jsp";
  String fldField2 = "LLinkNew.jsp";
  String fldField3 = "LAdminMenu.jsp";
  // Show fields

  oTpl.setVar ("Field1", fldField1);
  
  oTpl.setVar ("Field2", fldField2);
  
  oTpl.setVar ("Field3", fldField3);
  
  oTpl.parse ("FormMenu", false);
  
}

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(4);
    _jspx_dependants.add("/LTemplateObj.jsp");
    _jspx_dependants.add("/LCommon.jsp");
    _jspx_dependants.add("/LStyles.inc");
    _jspx_dependants.add("/LHeader.jsp");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Header.jsp", out, false);
      out.write('\r');
      out.write('\n');
      out.write('\r');
      out.write('\n');
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write('\r');
      out.write('\n');

    String errString = "";

      out.write("\r\n");
      out.write("<div id=\"Layer23\">\r\n");
      out.write("  <table width=\"693\" border=\"0\">\r\n");
      out.write("    <tr>\r\n");
      out.write("      <th width=\"683\" scope=\"col\"><div align=\"left\">\r\n");


java.sql.Connection conn = null;
java.sql.Statement  stat = null;
String sErr = loadDriver();
if ( ! sErr.equals("") ) {
 try {
   out.println(sErr);
 }
 catch (Exception e) {}
}
conn = cn();                  
stat = conn.createStatement();


String sAction =null;
String sForm =null;



session.putValue("sTreeErr","");

String sTemplateFileName = "LDefault.html";





TextTemplate oTpl = new TextTemplate();
String appPath = pageContext.getServletContext().getRealPath(request.getRequestURI()).replace('\\','/');
String cPath = request.getContextPath();
if ( cPath.length() > 1 ) {
  int f1 = appPath.lastIndexOf(cPath);
  appPath = appPath.substring(0,f1)+appPath.substring(f1+cPath.length());
}
appPath = appPath.substring(0,appPath.lastIndexOf('/')+1);
oTpl.loadTemplate(appPath+ sTemplateFileName , "main"); 

final String sHeaderFileName = appPath + "LHeader.html";
oTpl.loadTemplate(sHeaderFileName, "Header"); 

oTpl.setVar ("FileName", sFileName);


oTpl.setVar ("PageBODY", stylePageBODY);

oTpl.setVar ("FormTABLE", styleFormTABLE);

oTpl.setVar ("FormHeaderTD", styleFormHeaderTD);

oTpl.setVar ("FormHeaderFONT", styleFormHeaderFONT);

oTpl.setVar ("FieldCaptionTD", styleFieldCaptionTD);

oTpl.setVar ("FieldCaptionFONT", styleFieldCaptionFONT);

oTpl.setVar ("DataTD", styleDataTD);

oTpl.setVar ("DataFONT", styleDataFONT);

oTpl.setVar ("ColumnFONT", styleColumnFONT);

oTpl.setVar ("ColumnTD", styleColumnTD);


sAction = getParam(request, "FormAction", Text_TYPE);
sForm = getParam(request, "FormName", Text_TYPE);

if (sForm.equalsIgnoreCase("Tree")) {
  TreeAction( request, response, session, out, sForm, sAction, conn, stat, oTpl);
  if ("sendRedirect".equals(((String) session.getAttribute("sTreeErr"))) ) {
    if ( stat != null )  { stat.close(); stat = null; }
    if ( conn != null ) { conn.close(); conn = null; }
    return;
  }
}
Menu_Show( request, response, session, out, sForm, sAction, conn, stat, oTpl);
Search_Show( request, response, session, out, sForm, sAction, conn, stat, oTpl);
Lates_Show( request, response, session, out, sForm, sAction, conn, stat, oTpl);
Tree_Show( request, response, session, out, sForm, sAction, conn, stat, oTpl);
oTpl.parse("Header", false);
oTpl.parse("main", false);
out.print(oTpl.printVar("main"));


if ( stat != null )  { stat.close(); stat = null; }
if ( conn != null ) { conn.close(); conn = null; }


      out.write("<link rel=\"stylesheet\" href=\"css.css\"/>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("     \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</div></th>\r\n");
      out.write("    </tr>\r\n");
      out.write("  </table>\r\n");
      out.write("</div>\r\n");
      out.write("<div id=\"Layer1\"><img src=\"imges/Reklama-Majas-lapu-izstrade.jpg\" width=\"694\" height=\"344\" /></div>\r\n");
      out.write("\r\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "Addlogde.jsp", out, false);
      out.write('\r');
      out.write('\n');
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
